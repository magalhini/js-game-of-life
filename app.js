class Cell {
  constructor() {
    this.alive = Math.random() > .7;
    this.neighbours = 0;
  }
}

class Conway {
  constructor(size) {
    this.size = size;
    this.moves = [[-1,-1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];
    this.grid = this.makeGrid(size);
  }

  makeGrid(size) {
    let grid = [];
    for (let i = 0; i < size; i++) {
      let row = [];
      for (var j = 0; j < size; j++) {
        row.push(new Cell());
      }
      grid.push(row);
    }

    return grid;
  }

  show() {
    console.log('\x1Bc');
    for (let i = 0; i < this.size; i++) {
      const row = this.grid[i];
      let rowString = '';

      for (let j = 0; j < this.size; j++) {
        const cell = row[j];
        rowString += (cell.alive ? 'o|' : ' |');
      }
      console.log(rowString);
    }
  }

  isInBounds(r, c) {
    return r >= 0 && r < this.size && c >= 0 && c < this.size;
  }

  shouldLive(r, c) {
    const cell = this.grid[r][c];
    return !cell.alive && cell.neighbours === 3;
  }

  underPopulated(r, c) {
    const cell = this.grid[r][c];
    return cell.neighbours < 2;
  }

  overPopulated(r, c) {
    const cell = this.grid[r][c];
    return cell.neighbours > 3;
  }

  updateCellNeighbours(r, c) {
    const cell = this.grid[r][c];
    cell.neighbours = 0;

    for (let i = 0; i < this.moves.length; i++) {
      const direction = this.moves[i];
      const dr = direction[0];
      const dc = direction[1];

      if (this.isInBounds(r + dr, c + dc)) {
        const neighbour = this.grid[r + dr][c + dc];
        if (neighbour && neighbour.alive) {
          cell.neighbours++;
        }
      }
    }
  }

  updateState(r, c) {
    const cell = this.grid[r][c];
    if (this.underPopulated(r,c) || this.overPopulated(r,c)) {
      cell.alive = false;
    } else if (this.shouldLive(r,c)) {
      cell.alive = true;
    }
  }

  updateAllStates() {
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size; j++) {
        this.updateState(i, j);
      }
    }
  }

  updateAllNeighbours() {
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size; j++) {
        this.updateCellNeighbours(i, j);
      }
    }
  }
}

const game = new Conway(30);
const interval = setInterval(function() {
  game.show();
  game.updateAllNeighbours();
  game.updateAllStates();
}, 90);
